package io.mountblue;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    boolean flagSwitch = true;
    boolean showOutput = false;

    Button btnZero, btnOne, btnTwo, btnThree, btnFour, btnFive, btnSix, btnSeven, btnEight, btnNine, btnDot, btnGo, btnAc, btnBackspace;
    ConstraintLayout clWeight, clHeight, clKeypad, main;
    EditText etWeightValue, etHeightValue;
    LinearLayout weightUnitsLayout, heightUnitslayout;
    Spinner weightUnitDialog;
    TextView tvWeightUnit, tvHeightUnit;
    View viewKeypad, outputBMI;

    String weightUnit = "Kilogram", heightUnit = "Centimeter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        if(savedInstanceState != null && (savedInstanceState.getSerializable("showOutput") != null)) {
           showOutput = (boolean) savedInstanceState.getSerializable("showOutput");
           if(showOutput) {
               viewKeypad.setVisibility(View.INVISIBLE);
               outputBMI.setVisibility(View.VISIBLE);
           } else {
               viewKeypad.setVisibility(View.VISIBLE);
               outputBMI.setVisibility(View.INVISIBLE);
           }
        }

        weightUnitsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] weightUnits = getResources().getStringArray(R.array.weight_units);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setItems(weightUnits, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        weightUnit = weightUnits[which];
                        tvWeightUnit.setText(weightUnit);
                        if (showOutput) {
                            openKeypad();
                        }
                    }
                });
                builder.show();
            }
        });

        heightUnitslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] heightUnits = getResources().getStringArray(R.array.height_units);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setItems(heightUnits, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        heightUnit = heightUnits[which];
                        tvHeightUnit.setText(heightUnit);
                        if (showOutput) {
                            openKeypad();
                        }
                    }
                });
                builder.show();
            }
        });

        clWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagSwitch = true;
                etWeightValue.setTextColor(getResources().getColor(R.color.colorOrange));
                etHeightValue.setTextColor(Color.BLACK);
                if (showOutput) {
                    openKeypad();
                }
            }
        });

        clHeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagSwitch = false;
                etHeightValue.setTextColor(getResources().getColor(R.color.colorOrange));
                etWeightValue.setTextColor(Color.BLACK);
                if (showOutput) {
                    openKeypad();
                }
            }
        });

    }

    private void initViews() {
        btnZero = findViewById(R.id.btn_zero);
        btnOne = findViewById(R.id.btn_one);
        btnTwo = findViewById(R.id.btn_two);
        btnThree = findViewById(R.id.btn_three);
        btnFour = findViewById(R.id.btn_four);
        btnFive = findViewById(R.id.btn_five);
        btnSix = findViewById(R.id.btn_six);
        btnSeven = findViewById(R.id.btn_seven);
        btnEight = findViewById(R.id.btn_eight);
        btnNine = findViewById(R.id.btn_nine);
        btnDot = findViewById(R.id.btn_dot);
        btnGo = findViewById(R.id.btn_go);
        btnBackspace = findViewById(R.id.btn_cancel);
        btnAc = findViewById(R.id.btn_ac);

        etWeightValue = findViewById(R.id.et_weight_value);
        etHeightValue = findViewById(R.id.et_height_value);

        tvWeightUnit = findViewById(R.id.tv_weight_unit_value);
        tvHeightUnit = findViewById(R.id.tv_height_unit_value);

        clWeight = findViewById(R.id.cl_weight);
        clHeight = findViewById(R.id.cl_height);
        clKeypad = findViewById(R.id.cl_keypad);
        main = findViewById(R.id.main);

        heightUnitslayout = findViewById(R.id.ll_height_units);
        weightUnitsLayout = findViewById(R.id.ll_weight_units);
        weightUnitDialog = findViewById(R.id.spin_weight);

        viewKeypad = findViewById(R.id.cl_keypad);
        outputBMI = findViewById(R.id.layout_output);
    }

    public void onClickB(View view) {
        int id = view.getId();
        Button b = findViewById(id);

        switch (id) {
            case R.id.btn_ac:
                if (flagSwitch) {
                    etWeightValue.setText("0");
                } else {
                    etHeightValue.setText("0");
                }
                break;

            case R.id.btn_dot:
                if (flagSwitch) {
                    if (etWeightValue.getText().toString().equals("0")) {
                        etWeightValue.setText("0.");
                    } else if(etWeightValue.getText().toString().contains(".")) {

                    } else {
                        etWeightValue.setText(etWeightValue.getText() + ".");
                    }
                } else {
                    if (etHeightValue.getText().toString().equals("0")) {
                        etHeightValue.setText("0.");
                    } else if(etHeightValue.getText().toString().contains(".")) {

                    } else {
                        etHeightValue.setText(etHeightValue.getText() + ".");
                    }
                }
                break;

            case R.id.btn_cancel:
                if (flagSwitch) {
                    if (etWeightValue.getText().toString().equals("0")) {

                    } else {
                        String s = etWeightValue.getText().toString();
                        etWeightValue.setText(s.substring(0, s.length() - 1));
                        if (etWeightValue.getText().length() == 0) {
                            etWeightValue.setText("0");
                        }
                    }
                } else {
                    if (etHeightValue.getText().toString().equals("0")) {

                    } else {
                        String s = etHeightValue.getText().toString();
                        etHeightValue.setText(s.substring(0, s.length() - 1));
                        if (etHeightValue.getText().length() == 0) {
                            etHeightValue.setText("0");
                        }
                    }
                }
                break;

            case R.id.btn_go:
                String weightValue = etWeightValue.getText().toString();
                String heightValue = etHeightValue.getText().toString();
                calculateBMI(weightValue, weightUnit, heightValue, heightUnit);
                break;

            case R.id.btn_zero:
                if (flagSwitch) {
                    if (etWeightValue.getText().toString().equals("0") || etWeightValue.getText().length() == 3) {

                    } else if (etWeightValue.getText().length() < 3) {
                        etWeightValue.setText(etWeightValue.getText() + "0");
                    }
                } else {
                    if (etHeightValue.getText().toString().equals("0") || etHeightValue.getText().length() == 3) {

                    } else if (etHeightValue.getText().length() < 3) {
                        etHeightValue.setText(etHeightValue.getText() + "0");
                    }
                }
                break;

            default:
                if (flagSwitch) {
                    if (etWeightValue.getText().toString().equals("0")) {
                        etWeightValue.setText(b.getText());
                    } else if (etWeightValue.getText().length() < 3) {
                        etWeightValue.setText(etWeightValue.getText() + b.getText().toString());
                    } else if((etWeightValue.getText().length() == 3 || etWeightValue.getText().length() == 4) && etWeightValue.getText().toString().contains(".")) {
                        etWeightValue.setText(etWeightValue.getText() + b.getText().toString());
                    }
                } else {
                    if (etHeightValue.getText().toString().equals("0")) {
                        etHeightValue.setText(b.getText());
                    } else if (etHeightValue.getText().length() < 3) {
                        etHeightValue.setText(etHeightValue.getText() + b.getText().toString());
                    } else if((etHeightValue.getText().length() == 3 || etHeightValue.getText().length() == 4) && etHeightValue.getText().toString().contains(".")) {
                        etHeightValue.setText(etHeightValue.getText() + b.getText().toString());
                    }
                }
                break;
        }
    }

    public void calculateBMI(String weightVal, String weightUnit, String heightVal, String heightUnit) {
        double weight = Double.parseDouble(weightVal);
        double height = Double.parseDouble(heightVal);

        if (weightUnit.equals("Pound")) {
            weight = weight / 2.2046;
        }

        if (heightUnit.equals("Centimeter")) {
            height = height / 100;
        } else if (heightUnit.equals("Feet")) {
            height = height / 3.281;
        } else if (heightUnit.equals("Inch")) {
            height = height / 39.37;
        }

        double BMI = weight / (height * height);

        if (BMI < 1.0 || BMI > 60.0) {
            Toast.makeText(this, "Invalid BMI! Please check your input", Toast.LENGTH_SHORT).show();
        } else {
            openOutputView(BMI);
            showOutput = true;
        }
    }

    public void openOutputView(double BMI) {
        viewKeypad.setVisibility(View.INVISIBLE);
        outputBMI.setVisibility(View.VISIBLE);

        showOutput = true;

        TextView tvBmiValue = findViewById(R.id.tv_bmi_value);
        tvBmiValue.setText(String.format("%.1f", BMI));
        TextView tvBmiResult = findViewById(R.id.tv_bmi_result);
        if (BMI < 18.5) {
            tvBmiResult.setTextColor(getResources().getColor(R.color.colorUnderweight));
            tvBmiResult.setText(getResources().getString(R.string.underweight));
        } else if (BMI >= 18.5 || BMI < 20.5) {
            tvBmiResult.setTextColor(getResources().getColor(R.color.colorNormal));
            tvBmiResult.setText(getResources().getString(R.string.normal));
        } else if (BMI >= 25.0) {
            tvBmiResult.setTextColor(getResources().getColor(R.color.colorOrange));
            tvBmiResult.setText(getResources().getString(R.string.overweight));
        }
    }

    public void openKeypad() {
        viewKeypad.setVisibility(View.VISIBLE);
        outputBMI.setVisibility(View.INVISIBLE);
        showOutput = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putSerializable("showOutput", showOutput);
    }
}